/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package serve

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/slax0rr/go-web-scaffold/infrastructure/container"
	"gitlab.com/slax0rr/go-web-scaffold/infrastructure/server"
)

func Execute(srvrCfg server.Config) {
	logrus.WithFields(logrus.Fields{
		"address": srvrCfg.Addr,
		"port":    srvrCfg.Port,
	}).Debug("starting a http server")

	err := server.Start(srvrCfg, container.Handlers)
	if err != nil {
		logrus.WithError(err).Fatal("error occurred executing the HTTP server")
	}
}
