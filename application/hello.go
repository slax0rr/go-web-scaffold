/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package application

import (
	"context"

	"gitlab.com/slax0rr/go-web-scaffold/domain"
)

type Hello interface {
	Greet(ctx context.Context, lang, name string) (string, error)
}

// Hello application layer between the intarface and the domain implementation of hello.
type HelloImpl struct{}

// Compile-time check if HelloImpl implements Hello
var _ Hello = new(HelloImpl)

// Greet .
func (a *HelloImpl) Greet(ctx context.Context, lang, name string) (string, error) {
	hello := &domain.Hello{Lang: lang}
	return hello.GetGreeting(ctx, name)
}
