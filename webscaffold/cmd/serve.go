/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/slax0rr/go-web-scaffold/infrastructure/server"
	"gitlab.com/slax0rr/go-web-scaffold/interface/cli/serve"
)

var (
	serveAddr        string
	servePort        int
	serveSockFile    string
	serveTimeout     int
	serveHealthcheck bool
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start a web server",
	Long:  `Starts a web server of the go-web-scaffold application.`,
	Run: func(cmd *cobra.Command, args []string) {
		initServe()

		serve.Execute(server.Config{
			Addr:        viper.GetString("http.addr"),
			Port:        viper.GetInt("http.port"),
			SockFile:    viper.GetString("http.sockFile"),
			Timeout:     viper.GetInt("http.timeout"),
			Healthcheck: viper.GetBool("http.healthcheck"),
		})
	},
}

func init() {
	rootCmd.AddCommand(serveCmd)

	serveCmd.PersistentFlags().StringVar(&serveAddr, "listen", "", "overrides configured listen address")
	serveCmd.PersistentFlags().IntVar(&servePort, "port", 0, "override configured port number")
	serveCmd.PersistentFlags().StringVar(&serveSockFile, "socket", "", "override configured socket file path")
	serveCmd.PersistentFlags().IntVar(&serveTimeout, "timeout", -1, "override configured server shutdown timeout")
	serveCmd.PersistentFlags().BoolVar(&serveHealthcheck, "healthcheck", false, "enable healtcheck route")
}

func initServe() {
	if serveAddr != "" {
		viper.Set("http.addr", serveAddr)
	}

	if servePort > 0 {
		viper.Set("http.port", servePort)
	}

	if serveSockFile != "" {
		viper.Set("http.sockFile", serveSockFile)
	}

	if serveTimeout >= 0 {
		viper.Set("http.timeout", serveTimeout)
	}

	if serveHealthcheck {
		viper.Set("http.healthcheck", true)
	}
}
