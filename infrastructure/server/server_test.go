package server_test

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

type testHandler struct {
	handleFunc http.HandlerFunc
}

func (h *testHandler) Register(rtr *mux.Router) {
	rtr.HandleFunc("/test", h.handleFunc)
}

func (t *serverTestSuite) TestServerHealthcheck() {
	resp, err := http.DefaultClient.Get(fmt.Sprintf("http://127.0.0.1:%d/healthcheck", port))
	if !t.Nil(err, fmt.Sprintf("unable to do a healthcheck: %s", err)).Passed {
		return
	}

	if !t.Equal(http.StatusOK,
		resp.StatusCode,
		fmt.Sprintf("unexpected healthcheck statuscode: %d", resp.StatusCode),
	).Passed {
		return
	}
}

func (t *serverTestSuite) TestServerHandlers() {
	expResp := "test route"

	handler := &testHandler{}
	handler.handleFunc = http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprint(w, expResp)
	})

	srvr.AddHandler(handler)

	resp, err := http.DefaultClient.Get(fmt.Sprintf("http://127.0.0.1:%d/test", port))
	if !t.Nil(err, fmt.Sprintf("unable to reach test handler: %s", err)).Passed {
		return
	}
	if !t.Equal(http.StatusOK, resp.StatusCode).Passed {
		return
	}

	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if !t.Nil(err, fmt.Sprintf("unable to read response body: %s", err)).Passed {
		return
	}

	t.Equal(expResp, string(b))
}
