package server_test

import (
	"fmt"
	"net"
	"os"
	"testing"
	"time"

	"github.com/remogatto/prettytest"
	"gitlab.com/slax0rr/go-web-scaffold/infrastructure/server"
)

var (
	srvr     *server.Server
	port     int
	sockFile string
)

type serverTestSuite struct {
	prettytest.Suite
}

func getFreePort() (int, error) {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return 0, err
	}

	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}
	defer l.Close()
	return l.Addr().(*net.TCPAddr).Port, nil
}

func TestServerTestSuite(t *testing.T) {
	var err error
	port, err = getFreePort()
	if err != nil {
		t.Fatalf("unable to determine a free random port for the test server: %s\n", err)
	}

	srvr = &server.Server{}
	sockFile = fmt.Sprintf("/tmp/test-server-%d.sock", port)
	srvr.SetConfig(&server.Config{
		Port:        port,
		SockFile:    sockFile,
		Timeout:     10,
		Healthcheck: true,
	})

	srvr.InitRouter()
	go srvr.Serve()

	// wait for the server to start
	time.Sleep(100 * time.Millisecond)

	prettytest.RunWithFormatter(t,
		new(prettytest.TDDFormatter),
		new(serverTestSuite))

	// remove possible created socket file
	os.Remove(sockFile)

	err = srvr.Close()
	if err != nil {
		t.Fatalf("unable to stop running server: %s", err)
	}
}
