/*
Copyright © 2020 Tomaz Lovrec <tomaz.lovrec@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package server

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	httpserver "gitlab.com/slax0rr/go-httpserver"
)

// starter interface requires the Start method.
type starter interface {
	// Start is executed on the server before starting the listen process, if the server implements the starter interface.
	Start() error
}

// closer interface requires the Close method.
type closer interface {
	// Clo se is executed on the server after it stopped listening, if it implements the closer interface.
	Close()
}

// Config values for the HTTP server
type Config struct {
	// Address on which the server is listening
	Addr string

	// Port on which the server is listening
	Port int

	// Servers socket file which it uses for child communication
	SockFile string

	// Grace period for opened connections before the server is killed
	Timeout int

	// Enable the healthcheck route replying with 200 OK
	Healthcheck bool
}

// Handler represents a HTTP Handler which implements a Register function for registering itself with the router
type Handler interface {
	// Register the handler to the router
	Register(*mux.Router)
}

// Start the HTTP server.
func Start(cfg Config, handlers []Handler) error {
	srvr := new(Server)
	srvr.SetConfig(&cfg)
	srvr.SetHandlers(handlers)

	srvr.InitRouter()
	return srvr.Serve()
}

// Server struct containing the configuration and where the user can implement the starter and/or closer interfaces.
type Server struct {
	cfg      *Config
	handlers []Handler
	rtr      *mux.Router
}

// SetConfig writes the pointer to the config object to the server object.
func (srvr *Server) SetConfig(cfg *Config) {
	srvr.cfg = cfg
}

// SetHandlers writes the handler slice to the server object.
func (srvr *Server) SetHandlers(handlers []Handler) {
	srvr.handlers = handlers
}

// AddHandler registers a new handler with the router.
// If the router has not been initialised yet, the func panics.
func (srvr *Server) AddHandler(handler Handler) {
	if srvr.rtr == nil {
		panic("router not initialised")
	}

	handler.Register(srvr.rtr)
}

// Serve starts the HTTP listener.
// If the server implements the starter interface it calls the Start method, and if it implements the closer method it will execute Close after the listener stops.
func (srvr *Server) Serve() error {
	if srvr.rtr == nil {
		panic("router not initialised")
	}

	if starter, ok := interface{}(srvr).(starter); ok {
		err := starter.Start()
		if err != nil {
			return err
		}
	}

	defer func() {
		if closer, ok := interface{}(srvr).(closer); ok {
			closer.Close()
		}
	}()

	return httpserver.Serve(httpserver.Config{
		SockFile: srvr.cfg.SockFile,
		Addr:     fmt.Sprintf("%s:%d", srvr.cfg.Addr, srvr.cfg.Port),
		Timeout:  time.Duration(srvr.cfg.Timeout) * time.Second,
	}, srvr.rtr)
}

// InitRouter initialises the router, registers the healthcheck route, if the config permits it, and registers all defined handlers.
func (srvr *Server) InitRouter() {
	srvr.rtr = mux.NewRouter()

	if srvr.cfg.Healthcheck {
		// simple healthcheck route, returning 200 signaling the HTTP server has started up
		srvr.rtr.HandleFunc("/healthcheck", func(w http.ResponseWriter, req *http.Request) {})
	}

	for _, h := range srvr.handlers {
		h.Register(srvr.rtr)
	}
}

// Close stops the running server.
func (srvr *Server) Close() error {
	return httpserver.Stop(false)
}
