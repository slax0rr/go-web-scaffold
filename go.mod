module gitlab.com/slax0rr/go-web-scaffold

go 1.13

require (
	github.com/adams-sarah/prettytest v0.0.0-20150503061827-2880862ae7f5 // indirect
	github.com/adams-sarah/test2doc v0.0.0-20180225015401-dfbef56b3eab
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mitchellh/mapstructure v1.2.2 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pelletier/go-toml v1.7.0 // indirect
	github.com/remogatto/prettytest v0.0.0-20200211072524-6d385e11dcb8
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.5.1 // indirect
	gitlab.com/slax0rr/go-httpserver v0.0.0-20200413113638-87a412b410c8
	golang.org/x/sys v0.0.0-20200409092240-59c9f1ba88fa // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/ini.v1 v1.55.0 // indirect
)
