GOCMD=go
CURL=curl
DLV=dlv
REPO_PATH=gitlab.com/slax0rr/go-web-scaffold
CMD_PATH=${REPO_PATH}/webscaffold
BIN_PATH=${GOPATH}/bin
CMD_NAME=webscaffold
CFG_PATH=resources/config
CFG_FILE=${CFG_PATH}/webscaffold.yaml
DEBUG_LISTEN=127.0.0.1:40000
DOCS_FILE=docs/apiary

.PHONY: test build run debug docs prep

default: all

all: test build run

test:
	${GOCMD} test -v ./...

build:
	${GOCMD} mod tidy

	${GOCMD} build -o ${BIN_PATH}/${CMD_NAME} ${CMD_PATH}

run: build
	${BIN_PATH}/${CMD_NAME} serve --config ${CFG_FILE}

debug: EXEC_CMD ?= serve
debug:
	${DLV} debug \
		--listen ${DEBUG_LISTEN} \
		--headless \
		${CMD_PATH} -- \
			${EXEC_CMD} \
			--config ${CFG_FILE}

docs: test
	@cp ${DOCS_FILE}.tpl ${DOCS_FILE}.apib
	@find . -type f -not -path './docs/*' -name "*.apib" \
		\( -exec cat {} >> ${DOCS_FILE}.apib \; \
		-a -exec rm {} \; \)
	@echo "docs generated in '${DOCS_FILE}.apib'"

prep:
ifndef NEW_REPO_PATH
	@echo error: NEW_REPO_PATH is not set!
	exit 1
endif
ifndef NEW_CMD_NAME
	@echo error: NEW_CMD_NAME is not set!
	exit 1
endif
	@find -type f -not -path '*/\.git\/*' \
		\( -exec sed -i -e 's#${REPO_PATH}#${NEW_REPO_PATH}#' {} \; \
		-a -exec sed -i -e 's#${CMD_NAME}#${NEW_CMD_NAME}#' {} \; \)

	@mv ${CMD_NAME} ${NEW_CMD_NAME}
	@mv ${CFG_PATH}/${CMD_NAME}.yaml ${CFG_PATH}/${NEW_CMD_NAME}.yaml
